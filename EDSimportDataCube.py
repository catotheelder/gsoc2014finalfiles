
# coding: utf-8

# In[ ]:

class importDataCube():
    

    #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def __init__(self,
                 foldername = "/home/jon/Desktop/gsoc2014/",
                 filename = "VC-Raney-im-cube.raw", 
                 width=256, height = 200, channels = 1024):

        import matplotlib.pyplot as plt
        import numpy as np
        
        self.foldername = foldername
        self.filename = filename
        self.dataCubePathAndFile = foldername + filename 
        self.fileNamePlain = filename [:-4]
        
        self.width = width
        self.height = height
        self.channels = channels
        
        fileobj = open(self.dataCubePathAndFile, 'rb')
        data = np.fromfile(fileobj,dtype = np.uint8)
        data = np.reshape(data,(self.width, self.height, self.channels) )
        self.data = data

